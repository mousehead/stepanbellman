package main

import (
	"log"

	"../../internal/task"
)

const (
	// MinLvlHoursGeneral is an minimal required amount of hours Stepa should spend studying each level
	MinLvlHoursGeneral = 220
)

// different strategies of changing the level on every lesson
const (
	LvlUpTypeUnavailable  = iota // no feasible strategy, skipping the option
	LvlUpTypeNo           = iota // no need to levelup
	LvlUpTypeBeforeLesson = iota
	LvlUpTypeDuringLesson = iota
)

// PartialSolutionsCache [rB][lvl] is used to store pre-computed best solutions
// for (0, rB) intervals with the last lesson ending in lvl
// because they are accessed often
type PartialSolutionsCache map[int](map[int]task.Solution)

// verifyLvls returns false when solution (even partial solution)
func verifyLvls(currentLvl int, hoursPerLvl [task.MaxLvl]int,
	minHoursPerLevel [task.MaxLvl]int) bool {
	if hoursPerLvl[0]+hoursPerLvl[1] > task.AllHours-minHoursPerLevel[2] {
		// i.e. if levels 1 and 2 occupy > 1-maxLvl3% of the total time,
		// thus making it impossible to get enough level 3 training
		return false
	}
	if currentLvl > 1 && hoursPerLvl[0] < minHoursPerLevel[0] {
		// if we already moved to lvl 2 but didn't get enough lvl 1 training
		// so it's too late
		return false
	}
	if currentLvl > 2 && hoursPerLvl[1] < minHoursPerLevel[1] {
		// same principle
		return false
	}
	return true
}

// find OptionsNearBorder returns all available lessons that end at
// rBorder. this includes empty lessons
func findOptionsNearBorder(rBorder int, lvl int) []task.Lesson {
	// , len(task.MainTeacherTable.Teachers
	options := make([]task.Lesson, 0)
	if rBorder == 0 {
		return options
	}

	// adding all lessons that are available at that time
	for _, teacher := range task.MainTeacherTable.Teachers {
		dur := teacher.ClassDuration
		if rBorder <= teacher.AvailableTill &&
			rBorder-dur >= teacher.AvailableFrom &&
			rBorder-dur >= 0 &&
			teacher.HasLevel[lvl-1] {
			options = append(options, task.Lesson(teacher.ID))
		}
	}

	// Now adding empty lesson options.
	// Not to miss anything we will add all possible wait durations
	// including sleeping all the interval through
	for t := 1; t < rBorder+1; t++ {
		options = append(options, task.Lesson(-t))
	}
	return options

}

// getBestPartialSolution returns the best solution on the partial interval of (0, rBorder)
// that has level rBorderLvl in the end
// the Best Partial Solutions Cache should already have all solutions
// bestPSCache[x][any lvl] precalculated for all x = 0..rBorder-1
func getBestPartialSolution(bestPSCache *PartialSolutionsCache,
	rBorder int, rBorderLvl int, minLvlHrs [task.MaxLvl]int, costSign int) task.Solution {

	// Initial best solution. --waiting for all the interval (0, rB)
	// If no real solutions are valid (i.e. none respect minimal hour requirements)
	// this base solution is returned
	initLessons := []task.Lesson{}
	if rBorder > 0 {
		initLessons = []task.Lesson{task.Lesson(-rBorder)}
	}

	bestSolution := task.Solution{
		Lessons: initLessons,
		LvlUps:  [task.MaxLvl]int{0, task.AllHours, task.AllHours},

		HoursPerLvl:      [task.MaxLvl]int{0, 0, 0},
		HighestLessonLvl: 1,
		Knowledge:        0,
		Cost:             0,
	}

	// the following four fields is a little redundant,
	// but will save us some time when solving
	// NB: Solution.ValidateAndUpdate does not and should not use any of them for evaluating

	if rBorder == 0 {
		return bestSolution
	}

	options := findOptionsNearBorder(rBorder, rBorderLvl)
	// log.Println(options)

	for _, lesson := range options {
		// here we denote
		// as someVariable1 the values that correspond to the option and
		// as someVariable0 we denote values that correspond to the best cached solution
		// for the remaining interval

		dur1 := lesson.GetDuration()
		realKn1 := lesson.GetReal() // whether this is a real lesson with real knowledge
		availableLvls1 := lesson.GetAvailableLvls()
		price1 := lesson.GetPrice()

		// lastLvl0 := bestSolution0.LastLvl

		// for a level L>1 we may have two different options:
		// 1. joining it with a pre-Solution that ends at level L too
		// 2. joining it with a pre-Solution that ends at level L-1, adding a levelup
		preSolutionLvls := []int{rBorderLvl}
		if rBorderLvl > 1 {
			preSolutionLvls = append(preSolutionLvls, rBorderLvl-1)
		}

		for _, lvl0 := range preSolutionLvls {
			// if we took this options, we would join this with the best solution
			// for the remaining interval (0, dur0), called here bestSolution0
			dur0 := rBorder - dur1
			bestSolution0 := (*bestPSCache)[dur0][lvl0]

			kn0 := bestSolution0.Knowledge
			hoursPerLvl0 := bestSolution0.HoursPerLvl

			strategy := LvlUpTypeUnavailable

			if lvl0 == rBorderLvl {
				strategy = LvlUpTypeNo
			}

			// hoursRemain is a number of hours to spend on lvl0 until we can move on and level up
			// this variable is unused when we execute LvlUpTypeNo leveling strategy
			hoursRemain := 0
			earliestLvlUpPossible := 0

			if rBorderLvl > 1 && lvl0 == rBorderLvl-1 {
				hoursRemain = task.IntMax(0, minLvlHrs[lvl0-1]-bestSolution0.HoursPerLvl[lvl0-1])
				earliestLvlUpPossible = dur0 + hoursRemain

				if hoursRemain > dur1 {
					// too early to level up
					break
				}
				if hoursRemain == 0 {
					strategy = LvlUpTypeBeforeLesson
				}
				if availableLvls1[lvl0-1] && hoursRemain > 0 {
					strategy = LvlUpTypeDuringLesson
				}

			}

			// now we execute strategies, calculating for our candidate/option lesson:
			// 1. the gained knowledge kn1
			// 2. updated lvlUps
			// 3. updated hoursPerLvl
			hoursPerLvl := hoursPerLvl0    // this is copy cause hoursPerLvl is an array, not a slice
			lvlUps := bestSolution0.LvlUps // this is also copy
			kn1 := 0
			switch strategy {
			case LvlUpTypeNo:
				kn1 = dur1 * rBorderLvl
				hoursPerLvl[rBorderLvl-1] += dur1 * realKn1
			case LvlUpTypeBeforeLesson:
				kn1 = dur1 * rBorderLvl
				lvlUps[rBorderLvl-1] = earliestLvlUpPossible
				hoursPerLvl[rBorderLvl-1] += dur1 * realKn1
			case LvlUpTypeDuringLesson:
				kn1 = (hoursRemain)*(rBorderLvl-1) + (dur1-hoursRemain)*(rBorderLvl)
				lvlUps[rBorderLvl-1] = earliestLvlUpPossible
				hoursPerLvl[rBorderLvl-2] += hoursRemain * realKn1
				hoursPerLvl[rBorderLvl-1] += (dur1 - hoursRemain) * realKn1
			case LvlUpTypeUnavailable:
				continue
			default:
				log.Fatalln("fatal: invalid levelup strategy. this should never happen")
			}

			// the knowledge of potential solution is just a sum of that of pre-solution and gained knowledge
			kn := kn0 + kn1*realKn1

			highestLessonLvl := bestSolution0.HighestLessonLvl
			if realKn1 != 0 {
				// only a real lesson increases currentLvl
				highestLessonLvl = rBorderLvl
			}

			if !verifyLvls(highestLessonLvl, hoursPerLvl, minLvlHrs) {
				continue
			}

			cost := bestSolution0.Cost + price1

			// comparison order is by (kn, cost)
			// i.e. superior knowledge solution always beats an old solution
			// even when it's more expensive
			if kn > bestSolution.Knowledge || (kn == bestSolution.Knowledge && cost*costSign < bestSolution.Cost*costSign) {
				lessons0 := make([]task.Lesson, len(bestSolution0.Lessons))
				copy(lessons0, bestSolution0.Lessons)
				bestSolution = task.Solution{
					Lessons: append(lessons0, lesson),
					LvlUps:  lvlUps,

					HighestLessonLvl: highestLessonLvl,
				}
				bestSolution.ValidateAndUpdate()
			}

		}

	}
	return bestSolution
}

func solvePart1(minLvlHrs [task.MaxLvl]int, costSign int) task.Solution {
	bestPSCache := make(PartialSolutionsCache)

	// Filling the cache
	for rBorder := 0; rBorder < task.AllHours+1; rBorder++ {
		bestPSCache[rBorder] = make(map[int]task.Solution)
		for lvl := 1; lvl <= task.MaxLvl; lvl++ {
			bestPSCache[rBorder][lvl] = getBestPartialSolution(&bestPSCache, rBorder, lvl, minLvlHrs, costSign)
		}
	}
	sol := bestPSCache[task.AllHours][3]
	sol.ValidateAndUpdate()

	return sol
}

// solvePart2 finds solution with the most time possible spent on lvl 1
// and of the remaining the most spent on lvl 2
// any other order of priority can be achieved in similar fashion
func solvePart2(costSign int) task.Solution {
	// we first find the highest possible (within limitations) number of hours on lvl 1
	solution1 := solvePart1([task.MaxLvl]int{1024, 220, 220}, costSign)
	maxHrsLvl1 := solution1.HoursPerLvl[0]

	// then, fixing this number of hours, we find the highest possible number of hours on lvl 2
	solution12 := solvePart1([task.MaxLvl]int{maxHrsLvl1, task.AllHours - maxHrsLvl1 - 220, 220}, costSign)
	maxHrsLvl2 := solution12.HoursPerLvl[1]

	// finally, we find a solution that corresponds to those minimum hours on lvl1 and lvl2
	solution123 := solvePart1([task.MaxLvl]int{maxHrsLvl1, maxHrsLvl2, task.AllHours - maxHrsLvl1 - maxHrsLvl2}, costSign)

	return solution123
}
