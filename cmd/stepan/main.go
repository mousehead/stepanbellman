package main

import (
	"fmt"

	"../../internal/task"
)

func main() {

	task.MainTeacherTable.Load("data/teacher_table.json")
	// task.MainTeacherTable.PrintTable()

	sol := solvePart1([task.MaxLvl]int{220, 220, 1024}, 1)
	fmt.Println("Task 1, cheapest:")
	sol.PrettyPrint()

	sol = solvePart1([task.MaxLvl]int{220, 220, 1024}, -1)
	fmt.Println("Task 1, least cheap:")
	sol.PrettyPrint()

	sol = solvePart2(1)
	fmt.Println("Task 2a, cheapest:")
	sol.PrettyPrint()

	sol = solvePart2(-1)
	fmt.Println("Task 2a, least cheap:")
	sol.PrettyPrint()

	// fmt.Println(sol.Lessons)

}
