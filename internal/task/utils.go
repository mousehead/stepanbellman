package task

// IntMax returns the maximum of two int values
func IntMax(a int, b int) int {
	if a >= b {
		return a
	}
	return b
}

// IntMin returns the minimum of two int values
func IntMin(a int, b int) int {
	if a <= b {
		return a
	}
	return b
}
