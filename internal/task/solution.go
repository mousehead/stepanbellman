package task

import (
	"encoding/json"
	"fmt"
	"log"
)

// Solution is a sequence of lessons, as well as some other useful data
type Solution struct {
	Lessons []Lesson
	LvlUps  [MaxLvl]int

	// the following three fields is a little redundant,
	// but will save us some time when solving
	// NB: Solution.ValidateAndUpdate does not and should not use any of them for evaluating
	// It only updates them
	HoursPerLvl [MaxLvl]int
	Knowledge   int
	Cost        int

	// This one is not even updated by ValidateAndUpdate --
	// it's used by the solver
	// it's 3 if there's been at least one lvl3 lesson
	// 2, if there's been at least one lvl2 lesson
	// and 1 otherwise (even if all lessons are empty)
	// 0 is always an incorrect value
	HighestLessonLvl int
}

// PrettyPrint displays solution as an indented JSON text
func (sol *Solution) PrettyPrint() error {
	data, err := json.MarshalIndent(sol, "", "    ")
	if err != nil {
		log.Println("FAIL!")
		return err
	}
	fmt.Printf("%s\n", data)
	return nil
}

// ValidateAndUpdate calculates knowledge and cost of a solution/study plan
// and returns true, updating knowledge/cost values if it is a valid solution
// if a solution is invalid, it returns false and updates nothing
func (sol *Solution) ValidateAndUpdate() bool {
	knowledge := 0
	cost := 0

	currentLvl := 1
	cursor := 0 // current hour number in our little simulation

	hoursPerLvl := [MaxLvl]int{0, 0, 0}

	for _, lesson := range sol.Lessons {
		for lvl := 1; lvl <= MaxLvl; lvl++ {
			if cursor >= sol.LvlUps[lvl-1] {
				currentLvl = lvl
			}
		}

		duration := lesson.GetDuration()

		if lesson < 0 {
			// an empty lesson just moves the cursor, no need to calculate anything els
			cursor += duration
			continue
		}

		lessonEnd := cursor + duration

		availableFrom, availableTill := lesson.GetAvailableRange()
		availableLvls := lesson.GetAvailableLvls()

		if cursor < availableFrom || lessonEnd > availableTill {
			log.Println("invalid solution: lesson", lesson, "is unavailable at t =", cursor)
			return false
		}

		// one lesson can overlap over more than one level if a teacher can do it
		// we want to know all levels this current lesson can span
		minLevelForLesson := currentLvl
		maxLevelForLesson := currentLvl
		for lvl := currentLvl; lvl < MaxLvl; lvl++ {
			if sol.LvlUps[lvl] < lessonEnd { // if the levelup is scheduled before the lesson ends
				maxLevelForLesson = lvl + 1
			}
		}

		// if the lesson spans multiple levels, we'll calculate knowledge for it
		// as a sum of knowledge for different chunks
		for lvl := minLevelForLesson; lvl <= maxLevelForLesson; lvl++ {
			if !availableLvls[lvl-1] {
				// log.Println("lvlups=", sol.LvlUps, "cursor=", cursor, "lessonend=", lessonEnd)
				log.Println("invalid solution: teacher", lesson, "can't teach lvl", currentLvl)
				return false
			}

			chunkStart := IntMax(cursor, sol.LvlUps[lvl-1])
			chunkEnd := IntMin(lessonEnd, AllHours)
			if lvl < MaxLvl {
				chunkEnd = IntMin(lessonEnd, sol.LvlUps[lvl])
			}

			knowledge += (chunkEnd - chunkStart) * lvl // knowledge is proportional to the level
			hoursPerLvl[lvl-1] += (chunkEnd - chunkStart)
		}

		cost += lesson.GetPrice()

		cursor = lessonEnd
	}

	sol.Knowledge = knowledge
	sol.Cost = cost
	sol.HoursPerLvl = hoursPerLvl

	return true
}
