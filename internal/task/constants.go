package task

const (
	// MaxLvl is the maximum level that can exist in our scenario
	MaxLvl = 3

	// AllHours is a maximum number of hours a student can have
	AllHours = 61 * 24 // 1464
)
