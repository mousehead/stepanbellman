package task

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
)

// Teacher represents all the features we need to know about a teacher
type Teacher struct {
	ID            int
	Name          string
	AvailableFrom int
	AvailableTill int
	ClassDuration int
	ClassPrice    int
	HasLevel      [MaxLvl]bool
}

// TeacherTable is a slice of all existing teacher for our scenario
// For the sake of simplicity we'll only use one TeacherTable object
//   and make it global/exported
// We also don't encapsulate the slice although if we didn't trust
//   the Stepa app enough, we could
type TeacherTable struct {
	Teachers []Teacher
}

// Get gets a teacher from the table, even though Teachers is directly accessible
func (table TeacherTable) Get(i int) Teacher {
	if i < 0 || i >= len(table.Teachers) {
		log.Fatal("fatal: tried to get a non-existing teacher, this should never happen")
		return Teacher{}
	}
	return table.Teachers[i]
}

// PrintTable prints a TeacherTable to stdout
func (table TeacherTable) PrintTable() {
	for i, t := range table.Teachers {
		fmt.Println(i, t)
	}
}

// Dump saves Teachers slice to file in JSON format
func (table TeacherTable) Dump(filePath string) error {
	data, err := json.MarshalIndent(table.Teachers, "", "  ")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filePath, data, 0644)
	return err
}

// Load loads Teachers slice into TeacherTable, discarding any existing slice
func (table *TeacherTable) Load(filePath string) error {
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &(table.Teachers))
	return err
}

// MainTeacherTable is the only TeacherTable that exists for a student
var MainTeacherTable = TeacherTable{nil}

// Example:
// var MainTeacherTable = TeacherTable{
// 	[]Teacher{
// 		Teacher{0, "Иван", 0, 408, 1, 500, [MaxLevel]bool{true, false, false}},
// 		Teacher{1, "Петр", 48, 264, 24, 400, [MaxLevel]bool{true, false, false}},
// 	},
// }
