package task

// Lesson is a lesson code,
// non-negative numbers x signify a lesson with teacher with id = x
// negative numbers -x signify an empty lesson (rest) of duration of x hours
type Lesson int

// GetDuration returns the duration of a lesson
// by either matching it from a teacher table (for non-empty lessons)
// or by simply inferring the duration from a lesson value for empty lessons
func (l Lesson) GetDuration() int {
	if l < 0 {
		return int(-l)
	}
	dur := MainTeacherTable.Get(int(l)).ClassDuration
	return dur
}

// GetPrice returns the price of a lesson
// The price of just relaxing is always zero
func (l Lesson) GetPrice() int {
	if l < 0 {
		return 0
	}
	price := MainTeacherTable.Get(int(l)).ClassPrice
	return price
}

// GetReal returns 1 if the lesson is real, and 0 if it's empty, i.e. relaxing
func (l Lesson) GetReal() int {
	if l < 0 {
		return 0
	}
	// we do it just to trigger fatal error if there's no such teacher:
	MainTeacherTable.Get(int(l))
	return 1
}

// GetAvailableRange returns an (AvailableFrom, AvailableTill) tuple
func (l Lesson) GetAvailableRange() (int, int) {
	if l < 0 {
		return 0, AllHours // relax is always available
	}
	// we do it just to trigger fatal error if there's no such teacher:
	t := MainTeacherTable.Get(int(l))
	return t.AvailableFrom, t.AvailableTill
}

// GetAvailableLvls returns a slice of bools with levels available at that lesson
func (l Lesson) GetAvailableLvls() [MaxLvl]bool {
	if l < 0 {
		return [MaxLvl]bool{true, true, true} // you can relax at any level
	}
	// we do it just to trigger fatal error if there's no such teacher:
	t := MainTeacherTable.Get(int(l))
	return t.HasLevel
}
